# setting a stage with the url s3://snowflake-workshop-lab/citibike-trips
# the file_format CSV and set the on error to skip leaving us with 61M rows

create stage CITIBIKE
url = 's3://snowflake-workshop-lab/citibike-trips';

use role LOADER;
use warehouse LOADING;  
use database RAW;

create or replace table BIKES.CITI_BIKES
(tripduration integer,
  starttime timestamp,
  stoptime timestamp,
  start_station_id integer,
  start_station_name string,
  start_station_latitude float,
  start_station_longitude float,
  end_station_id integer,
  end_station_name string,
  end_station_latitude float,
  end_station_longitude float,
  bikeid integer,
  membership_type string,
  usertype string,
  birth_year integer,
  gender integer);


list @CITIBIKE;

copy into BIKES.CITI_BIKES from @CITIBIKE
file_format=CSV
ON_ERROR = CONTINUE
;

select count(*) from RAW.BIKES.CITI_BIKES;


# next part create and load the json data
use role LOADER;
use warehouse LOADING;  
use database RAW;
create table BIKES.WEATHER (v variant);

create stage NYC_WEATHER
url = 's3://snowflake-workshop-lab/weather-nyc';


list @NYC_WEATHER;

copy into BIKES.WEATHER
from @NYC_WEATHER
file_format = (type=json);

select count(*) from RAW.BIKES.WEATHER;