{{ 
    config(materialized='table', database='analytics') 
}}

-- this assigns a weather for any given hour, could add tempature, wind etc if needed

select 
    weather
    , date
from
(
    select
        weather 
        , date_trunc('hour', time) as date
        , count(*) as n
        , row_number() over (partition by date order by n desc) rank_number
    from {{ source('rds', 'rds_weather') }}  
    group by 1, 2

) where rank_number = 1
