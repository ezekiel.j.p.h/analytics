{{ 
    config(materialized='table', database='analytics') 
}}

-- this aggregates by hour the metrics of distance, time and totals 

select 
    date_trunc('hour', starttime) as date
    , count(*) as trips_count
    , sum(tripduration) as duration_total
    , avg(tripduration) as duration_average
    , sum(haversine(
            start_station_latitude
            , start_station_longitude
            , end_station_latitude
            , end_station_longitude
            )) as distance_total
    , avg(haversine(
            start_station_latitude
            , start_station_longitude
            , end_station_latitude
            , end_station_longitude
            )) as distance_average

 from {{ source('rds', 'rds_bikes') }} 
 group by 1