{{ 
    config(materialized='table', database='analytics') 
}}

select
bikes.*
, weather
from {{ ref('bikes_metrics') }} bikes

left join {{ ref('weather_metrics') }} weather on bikes.date = weather.date

