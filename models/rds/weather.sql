{{ 
    config(
    alias='rds_weather'
    , materialized='table'
    ) 

}}

with source_weather as (
    select 
        parse_json(V):city:findname as name 
        , parse_json(V):weather[0].main as weather
        , parse_json(V):wind:speed as wind_speed
        , parse_json(V):main:temp as tempature
        , parse_json(V):time::timestamp as time
    
    from {{ source('bikes','weather')}}
) 

select * from source_weather