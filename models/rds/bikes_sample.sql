{{ 
    config(
    alias='rds_bikes_sample'
    , materialized='table'
    ) 

}}


with source_bikes as (
select    
 tripduration
,  starttime
,  stoptime
,  start_station_id
,  start_station_name
,  start_station_latitude
,  start_station_longitude
,  end_station_id
,  end_station_name
,  end_station_latitude
,  end_station_longitude
,  bikeid
,  membership_type
,  usertype
,  birth_year
,  gender

from {{ source('bikes','citi_bikes') }} sample (10) -- take 10% of the results
)

select * from source_bikes
